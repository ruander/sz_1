#Színek kezelése

##Színrendszerek

- HSL - hue saturation lightness
- CMYK - Cyan Magenta Yellow 
- RGB - Red Green Blue
- LAB - Luminosity A és B

###RGB - Red Green Blue
R- 0-255 | 00000000 - 11111111 8bit

G- 0-255 | 00000000 - 11111111 8bit

B- 0-255 | 00000000 - 11111111 8bit

Maximális színek száma:
00000000 11111111 00000000 /24bites szin (3x 8bites szín)
256^3 ~ 16,7 millió szín

~emberi szem színkülönség felismerési értékei átlagosan: 
-pár százezer

- rgb(0,255,0)
- rgba(r,g,b,alpha)
- \#00ff00 - hex


